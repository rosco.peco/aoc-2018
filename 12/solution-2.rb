#!/usr/bin/env ruby
#
# AoC 2018 Day 12 part 2
# (c)2018 Ross Bamford
# See LICENSE
#
# Wait for the glider...

if $0 == __FILE__
  STABLE_DIFFS = 20

  input = File.read(ARGV[0] || 'input.txt')

  if input =~ /initial state: ([#.]+)/
    current = $1.split(//).map { |c| c == '#' }

    rules = input.scan(/([.#]+) => ([#.])/).inject({}) do |h, (rule, result)|
      h[rule.split(//).map { |c| c == '#' }] = result == '#'
      h
    end

    sums, left_idx, ff, changes = [0], 0, [false] * 4, [0] * STABLE_DIFFS

    1.step do |i|
      current = (ff + current + ff).each_cons(5).map { |c| rules.fetch(c) }
      left_idx -= 2
      sums << current.zip(left_idx.step).inject(0) do |sum, (plant, idx)| 
        sum + if plant
          idx
        else
          0
        end
      end

      changes.shift
      changes << sums[-1] - sums[-2]
      sums.shift 

      if changes.uniq.length == 1
        puts sums[-1] + changes[0] * (50 * 10 ** 9 - i)
        break
      end
    end

  else
    puts "Bad input"
  end
end

