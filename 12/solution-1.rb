#!/usr/bin/env ruby
#
# AoC 2018 Day 12 part 1
# (c)2018 Ross Bamford
# See LICENSE

if $0 == __FILE__
  input = File.read(ARGV[0] || 'input.txt')

  if input =~ /initial state: ([#.]+)/
    current = $1.split(//).map { |c| c == '#' }

    rules = input.scan(/([.#]+) => ([#.])/).inject({}) do |h, (rule, result)|
      h[rule.split(//).map { |c| c == '#' }] = result == '#'
      h
    end

    sum, left_idx, ff = 0, 0, [false] * 4 

    20.times do
      current = (ff + current + ff).each_cons(5).map { |c| rules.fetch(c) }
      left_idx -= 2
      sum = current.zip(left_idx.step).inject(0) do |sum, (plant, idx)| 
        sum + if plant
          idx
        else
          0
        end
      end
    end

    puts sum
  else
    puts "Bad input"
  end

end

