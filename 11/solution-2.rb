#!/usr/bin/env ruby
#
# AoC 2018 Day 11 part 2
# (c)2018 Ross Bamford
# See LICENSE
#
# Horrible brute force, I just ran until the power stabilised and then submitted.
# Could be optimized in a number of ways, but I couldn't get anything better working
# quickly enough so this did the job for now ;) 

if $0 == __FILE__
  SERIAL = 9110

  def cell_power_level(serial,x,y)
    rack_id = x + 10
    power_level = rack_id * y
    power_level += serial
    power_level *= rack_id
    power_level = if (power_level < 100)
      0
    else
      power_level.to_s[-3].to_i
    end
    power_level - 5
  end

  def square_power(order)
    highx, highy, high = 0, 0, -1

    (300-order).times do |y|
      (300-order).times do |x|
        chigh = (0...order).inject(0) { |sum, sy| (0...order).inject(sum) { |sum, sx| sum + cell_power_level(SERIAL, x + sx, y + sy) } }
        if chigh > high
          high = chigh
          highx = x
          highy = y
        end
      end
    end

    [order, highx, highy, high]
  end

  highest, highestx, highesty, highestorder = 0,0,0,0

  order, x, y, power = *(1..300).map do |order| 
    ho, hx, hy, hp = *square_power(order) 
    if hp > highest
      highest = hp
      highestorder = ho
      highestx = hx
      highesty = hy
    end
    puts "#{highestorder}x#{highestorder}: high: #{highest} (#{highestx}, #{highesty})"
    [ho,hx,hy,hp]
  end.sort_by(&:last).last

  puts "Most powerful is #{order}x#{order} at #{x},#{y} with power #{power}"
end

