#!/usr/bin/env ruby
#
# AoC 2018 Day 11 part 1
# (c)2018 Ross Bamford
# See LICENSE

if $0 == __FILE__
  SERIAL = 9110

  def cell_power_level(serial,x,y)
    rack_id = x + 10
    power_level = rack_id * y
    power_level += serial
    power_level *= rack_id
    power_level = if (power_level < 100)
      0
    else
      power_level.to_s[-3].to_i
    end
    power_level - 5
  end

  power_levels = (0..297).inject([]) do |levels, y|
    levels << (0..297).inject([]) do |levels, x|
      levels << cell_power_level(SERIAL, x, y)     + cell_power_level(SERIAL, x + 1, y)     + cell_power_level(SERIAL, x + 2, y)     +
                cell_power_level(SERIAL, x, y + 1) + cell_power_level(SERIAL, x + 1, y + 1) + cell_power_level(SERIAL, x + 2, y + 1) +
                cell_power_level(SERIAL, x, y + 2) + cell_power_level(SERIAL, x + 1, y + 2) + cell_power_level(SERIAL, x + 2, y + 2)
    end
  end

  high, highx, highy = 0,0,0
  
  (0..297).each do |y|
    (0..297).each do |x|
      if power_levels[y][x] > high
        highx = x
        highy = y
        high = power_levels[y][x]
      end
    end
  end

  puts "Most powerful 3x3 starts at #{highx},#{highy}"
end

