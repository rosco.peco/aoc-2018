#!/usr/bin/env ruby
#
# AoC 2018 Day 13 part 1
# (c)2018 Ross Bamford
# See LICENSE
module AoC
  module Day13
    CORNER_CHANGE_MATRIX = {
      '<' => { 
          '/'  => 'v',
          '\\' => '^'
        },
      '>' => {
          '/'  => '^',
          '\\' => 'v'
        },
      '^' => {
          '/'  => '>',
          '\\' => '<'
        },
      'v' => {
          '/'  => '<',
          '\\' => '>'
        }
    }

    INTERSECTION_CHANGE_MATRIX = {
      '<' => {
          '<' => 'v',
          '^' => '<',
          '>' => '^'
        },
      '>' => {
          '<' => '^',
          '^' => '>',
          '>' => 'v'
        },
      '^' => {
          '<' => '<',
          '^' => '^',
          '>' => '>',
        },
      'v' => {
          '<' => '>',
          '^' => 'v',
          '>' => '<'
        }
    }

    class Cart
      attr_reader :x, :y, :direction, :xdelta, :ydelta, :playfield

      def initialize(x, y, direction, playfield)
        @x, @y, @xdelta, @ydelta, @playfield = x, y, 0, 0, playfield
        @turns = ['<', '^', '>']
        set_direction(direction)
      end

      def set_direction(dir)
        case @direction = dir
        when '<' then @xdelta, @ydelta = -1, 0
        when '>' then @xdelta, @ydelta = 1, 0
        when 'v' then @xdelta, @ydelta = 0, 1
        when '^' then @xdelta, @ydelta = 0, -1
        end
      end

      def move!
        this_cell = @playfield[y][x]
        if "/\\".include?(this_cell)
          # on a corner
          set_direction(CORNER_CHANGE_MATRIX[direction][this_cell])
        elsif '+'.eql?(this_cell)
          # intersection
          turn = @turns.shift
          set_direction(INTERSECTION_CHANGE_MATRIX[direction][turn])
          @turns << turn
        end

        [@x += @xdelta, @y += @ydelta]
      end

      def inspect
        "CART: #{x}, #{y} (#{xdelta}, #{ydelta}) [#{direction}]"
      end
    end
  end
end
 
if $0 == __FILE__
  require 'pp'

  include AoC::Day13

  if VISUAL = ARGV.delete('--visual')
    def draw_playfield(playfield, carts)
      playfield.each_with_index do |line, y|
        line.each_with_index do |cell, x|
          cart = carts.find_all { |cart| cart.x == x && cart.y == y }
          if cart.empty?
            print cell
          else
            if cart.length > 1
              print "X"
            else
              print cart.first.direction
            end
          end
        end
        puts
      end
      print "\e[#{playfield.length}A\r"
    end
  end

  playfield = File.read(ARGV[0] || 'input.txt').each_line.map { |l| l.chomp.split('') }
  raise "Bad input" unless playfield.all? { |l| l.length == playfield[0].length }

  # find carts
  carts = playfield.each_with_index.inject([]) do |carts, (line, y)|
    line.each_with_index.inject(carts) do |carts, (cell, x)|
      if '^v<>'.include? cell
        carts << Cart.new(x, y, cell, playfield)
      else
        carts
      end
    end
  end

  # Remove carts from playfield
  carts.each { |cart| playfield[cart.y][cart.x] = '<>'.include?(cart.direction) ? '-' : '|' }
  puts "Starting with #{carts.length} carts"

  loop do
    sorted_carts = carts.inject([]) { |a,cart| (a[cart.y] ||= []) << cart; a }.compact.map { |a| a.sort_by(&:x) }.flatten

    sorted_carts.each do |cart| 
      move = cart.move!

      crashed_carts = sorted_carts.map { |cart| [cart.x, cart.y] }.select { |a| a == move }
        
      if crashed_carts.length > 1
        sorted_carts.delete_if { |cart| cart.x == move.first && cart.y == move.last }
        puts "Crash at #{move.first},#{move.last} (#{sorted_carts.length} cart(s) left running)"
      end
    end

    # if visual mode, draw before removing crashed carts so we show X
    if VISUAL
      draw_playfield(playfield, carts)
      sleep 0.5
    end

    if carts.length == 1
      puts "Last cart at #{carts.first.x},#{carts.first.y} (#{carts.first.direction})"
      break
    end

    break if carts.empty? 

    carts = sorted_carts
  end
end

