#!/usr/bin/env ruby
#
# AoC 2018 Day 2 part 1
# (c)2018 Ross Bamford
# See LICENSE
#

module AoC
  module Day2    
    class << self
      def count_dup_twos_and_threes(input_lines)
        input_lines.inject([0,0]) do |(two, three), s|
          hash = s.split(//).inject(Hash.new { |h,k| h[k] = 0 }) { |h,c| h[c] += 1 ; h }
          [
            two   + (hash.values.any? { |i| i == 2 } ? 1 : 0), 
            three + (hash.values.any? { |i| i == 3 } ? 1 : 0)
          ]
        end
      end
    end
  end
end

if $0 == __FILE__
  num_twos, num_threes = *AoC::Day2::count_dup_twos_and_threes(File.read(ARGV[0] || 'input.txt').split)
  puts "checksum = #{num_twos} * #{num_threes} == #{num_twos * num_threes}"
end

