#!/usr/bin/env ruby
#
# AoC 2018 Day 2 part 2
# (c)2018 Ross Bamford
# See LICENSE
#

module AoC
  module Day2
    class << self
      # N.B. expects all input strings are the same length!
      def each_fuzzy_dup(input)
        a = []
        input[0].length.times { a << {} }

        input.each do |line|
          line.length.times do |i|
            substr = if i > 0
              line[0..i-1] + line[i+1..-1]
            else
              line[1..-1]
            end

            if a[i][substr]
              # Seen before
              yield substr
            else
              a[i][substr] = true
            end
          end
        end
      end
    end
  end
end

if $0 == __FILE__
  AoC::Day2::each_fuzzy_dup(File.read(ARGV[0] || 'input.txt').split) do |dup|
    # Only need to print the first match
    puts dup
    exit!
  end
end

