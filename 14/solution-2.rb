#!/usr/bin/env ruby
#
# AoC 2018 Day 14 part 2
# (c)2018 Ross Bamford
# See LICENSE

module AoC
  module Day14
    class Recipe
      class << self
        def total_recipes
          @total_recipes ||= 0
        end

        def total_recipes=(r)
          @total_recipes = r
        end
      end
      
      attr_reader :score
      attr_accessor :n, :p

      def initialize(score, p = self, n = p)
        Recipe.total_recipes += 1
        @score, @p, @n = score, p, n
        yield self if block_given?
      end

      def insert_after(recipe)
        self.p = recipe
        self.n = recipe.n
        recipe.n.p = self
        recipe.n = self
        self
      end

      def each
        n = self
        loop do
          yield n
          n = n.n
          return if n.object_id == self.object_id
        end
      end

      def inspect 
        "[RECIPE(#{score})]"
      end

    end

    class Elf
      attr_reader :current_recipe

      def initialize(recipe)
        @current_recipe = recipe
      end

      def move!
        (0..current_recipe.score).map { @current_recipe = @current_recipe.n }
      end
    end

    class Kitchen
      attr_reader :scoreboard, :elves

      def initialize(initial_scores, num_elves)
        if initial_scores.length > 0
          @scoreboard = Recipe.new(initial_scores.shift)
        end

        current = @scoreboard
        initial_scores.each do |score|
          current = Recipe.new(score).insert_after(current)
        end
        
        @elves = (1..num_elves).map { Elf.new(current = current.n) }
      end

      def cook!(target, visual = false)
        target = target.split('').map(&:to_i)
        digits_to_match = target.dup

        catch(:done) do
          loop do
            @elves.map { |elf| elf.current_recipe.score }.sum.to_s.split(//).map(&:to_i).each do |digit|
              Recipe.new(digit).insert_after(@scoreboard.p)

              if digit == digits_to_match.first
                digits_to_match.shift
                throw(:done, Recipe.total_recipes - target.length) if digits_to_match.empty?
              else
                digits_to_match = target.dup
              end
            end

            @elves.each { |elf| elf.move! }

            if visual
              puts digits_to_match.inspect
              @scoreboard.each do |recipe|
                print recipe.score
              end
              puts
            end
          end
        end
      end
    end
  end
end

if $0 == __FILE__
  include AoC::Day14

  VISUAL = ARGV.delete('--visual')
  TARGET = ARGV[0] || "909441"

  kitchen = Kitchen.new([3, 7], 2)

  puts "Result: #{kitchen.cook!(TARGET)}"
end

