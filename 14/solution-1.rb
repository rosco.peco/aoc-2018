#!/usr/bin/env ruby
#
# AoC 2018 Day 14 part 1
# (c)2018 Ross Bamford
# See LICENSE

module AoC
  module Day14
    class Recipe
      class << self
        def total_recipes
          @total_recipes ||= 0
        end

        def total_recipes=(r)
          @total_recipes = r
        end
      end

      attr_reader :score
      attr_accessor :n, :p

      def initialize(score, p = self, n = self)
        Recipe.total_recipes += 1
        @score, @p, @n = score, p, n
        yield self if block_given?
      end

      def insert_after(recipe)
        self.p = recipe
        self.n = recipe.n
        recipe.n.p = self
        recipe.n = self
      end

      def each
        n = self
        loop do
          yield n
          n = n.n
          return if n.object_id == self.object_id
        end
      end

      def inspect 
        "[RECIPE(#{score})]"
      end

    end

    class Elf
      attr_reader :current_recipe

      def initialize(recipe)
        @current_recipe = recipe
      end

      def move!
        (0..current_recipe.score).map { @current_recipe = @current_recipe.n }
      end
    end
  end
end

if $0 == __FILE__
  include AoC::Day14

  VISUAL = ARGV.delete('--visual')
  N = (ARGV[0] || 909441).to_i

  scoreboard = Recipe.new(3) do |r|
    r.n = Recipe.new(7, r)
    r.n.p = r
    r.n.n = r
    r.p = r.n
  end

  elves = [Elf.new(scoreboard), Elf.new(scoreboard.n)]

  loop do
    break if Recipe.total_recipes >= N + 10

    if VISUAL
      scoreboard.each do |recipe|
        print recipe.score
      end
      puts
    end

    elves.map { |elf| elf.current_recipe.score }.sum.to_s.split(//).map(&:to_i).each { |digit| Recipe.new(digit).insert_after(scoreboard.p) }
    elves.each { |elf| elf.move! }
  end

  (N + 10 - Recipe.total_recipes).times do |i|
    puts "Rewinding #{i}"
    scoreboard = scoreboard.p
  end

  last_ten = (1..10).inject([scoreboard,[]]) do |(sb,a),i|
    sb = sb.p
    [sb, a << sb.score]
  end.last.reverse!

  puts "Result: #{last_ten.join}"
end

