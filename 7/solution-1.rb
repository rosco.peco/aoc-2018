#!/usr/bin/env ruby
#
# AoC 2018 Day 7 part 1
# (c)2018 Ross Bamford
# See LICENSE
#
if $0 == __FILE__
  graph, todo = File.read('input.txt')
    .scan(/Step ([A-Z]) must be finished before step ([A-Z]) can begin./)
    .inject([{},[]]) do |(graph, todo),(dep, step)| 
      (graph[step] ||= []) << dep
      todo << dep << step
      [graph, todo]
    end

  todo.uniq!.sort!.each { |task| graph[task] ||= [] }
  
  while !todo.empty?
    next_todo = todo.find { |task| graph[task].empty? }
    print next_todo
    graph.each { |k,v| v.delete(next_todo) }
    todo.delete(next_todo)
  end

  puts
end

