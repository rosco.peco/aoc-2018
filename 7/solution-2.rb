#!/usr/bin/env ruby
#
# AoC 2018 Day 7 part 2
# (c)2018 Ross Bamford
# See LICENSE
#
module AoC
  module Day7
    class Worker
      attr_reader :id, :task

      def initialize(id, task_TASK_TIMES, delay = 0)
        @id, @task_TASK_TIMES, @delay, @seconds = id, task_TASK_TIMES, delay, 0
      end

      def start_task(task, &on_complete)
        @task, @seconds, @on_complete = task, @task_TASK_TIMES[task], on_complete
      end

      def tick
        if @seconds > 0
          @seconds -= 1
          if @seconds == 0
            @on_complete.call(self)
            @on_complete == nil
            @task == nil
          end

          if @delay > 0
            sleep(@delay)
          end
        end
      end

      def free?
        @seconds == 0
      end

      def to_s
        "Worker [#{id}]; free? = #{free?} " + (free? ? "<idle>" : "task: #{@task} (#{@seconds} work remaining)")
      end
    end
  end
end

if $0 == __FILE__
  include AoC::Day7

  # Base time (upon which the task TASK_TIMES are based)
  BASE_TIME = 60

  # Worker count
  NUM_WORKERS = 5

  # Whether to use visual mode (slow, shows work being done per worker)
  VISUAL_MODE = ARGV.delete('--visual')
  
  # Delay to use in visual mode
  VISUAL_DELAY = 0.01
  
  # Time taken per task (seconds)
  TASK_TIMES = ('A'..'Z').each_with_index.inject({}) { |h, (c,i)| h[c] = i + BASE_TIME + 1; h }

  graph, todo = File.read('input.txt')
    .scan(/Step ([A-Z]) must be finished before step ([A-Z]) can begin./)
    .inject([{},[]]) do |(graph, todo),(dep, step)| 
      (graph[step] ||= []) << dep
      todo << dep << step
      [graph, todo]
    end

  todo.uniq!.sort!.each { |task| graph[task] ||= [] }
  free_workers = (0...NUM_WORKERS).map { |i| Worker.new(i, TASK_TIMES, (VISUAL_MODE ? VISUAL_DELAY : 0)) }
  all_workers = free_workers.dup
  total_seconds = 0

  if VISUAL_MODE
    puts "Graph  : #{graph.inspect}"
    puts "Todo   : #{todo.inspect}"
    puts "Workers: #{all_workers.length}"
    puts "=" * 70
  end

  until todo.empty? && free_workers.length == NUM_WORKERS
    free_at_start = free_workers.length

    while next_task = todo.find { |task| graph[task].empty? } and next_worker = free_workers.shift
      todo.delete(next_task)
      next_worker.start_task(next_task) do |worker|
        graph.each { |k,v| v.delete(worker.task) }
        free_workers << worker
      end
    end

    all_workers.each { |worker| worker.tick }
    total_seconds += 1

    if VISUAL_MODE
      print "#{all_workers.join("#{' ' * 30}\n")}#{' ' * 30}\nElapsed: #{total_seconds}\e[#{all_workers.length}A\e[100D"
    end
  end

  if VISUAL_MODE
    print "\e[#{all_workers.length+1}B"
  end
  puts "Total seconds: #{total_seconds}"
end

