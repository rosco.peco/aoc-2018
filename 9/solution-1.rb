#!/usr/bin/env ruby
#
# AoC 2018 Day 9 part 1
# (c)2018 Ross Bamford
# See LICENSE
#
module AoC
  module Day9
    class Marble
      attr_reader :num
      attr_accessor :prev, :next

      def initialize(num)
        @num = num
      end
  
      def insert(after, before)
        after.next = before.prev = self
        self.prev, self.next = after, before
        self
      end

      def remove!
        self.prev.next = self.next
        self.next.prev = self.prev
      end
    end
  end
end

if $0 == __FILE__
  include AoC::Day9

  if File.read(ARGV[0] || 'input.txt').match(/(\d+) players; last marble is worth (\d+) points/)
    scores = [0] * $1.to_i
    final_score = $2.to_i

    first = Marble.new(0)
    current = first.prev = first.next = first
    num = 0
    
    move = lambda do |num|
      if num % 23 == 0
        scores[num % scores.length] += num
        7.times { current = current.prev }
        current.remove!
        scores[num % scores.length] += current.num
        current = current.next
      else
        current = Marble.new(num).insert(current.next, current.next.next)
      end
    end

    final_score.times { |num| move.call(num) }
    puts "Highest score: #{scores.max}"
  else
    puts "Bad input"
  end
end
