#!/usr/bin/env ruby
#
# AoC 2018 Day 4 part 2
# (c)2018 Ross Bamford
# See LICENSE
#
require 'date'
require 'pp'

module AoC
  module Day4
    FALLS_ASLEEP = "falls asleep"
    WAKES_UP     = "wakes up"

    class Guard
      attr_reader :id, :sleep_minutes, :total_minutes_sleep

      def initialize(id)
        @id, @sleep_minutes, @total_minutes_sleep = id, [0] * 60, 0
      end

      def log_sleep!(start_minute, end_minute)
        (start_minute...end_minute).each { |m| @sleep_minutes[m] += 1 }
        @total_minutes_sleep += end_minute - start_minute
      end

      def inspect
        "Guard ##{id}: asleep for #{total_minutes_sleep}"
      end
    end
  end
end

if $0 == __FILE__
  include AoC::Day4

  guards = Hash.new { |h,k| h[k] = Guard.new(k) }
  current_guard = nil
  current_sleep_start = nil

  # Read and sort input
  sorted_lines = File.read(ARGV[0] || 'input.txt').each_line.map do |line|
    if line =~ /\[([^\]]+)\]\s(.*)/
      date_str, event_str = $1, $2

      if event_str =~ /Guard\s#(\d+)/
        [DateTime.parse(date_str), $1]
      else
        [DateTime.parse(date_str), event_str]
      end
    end
  end.compact.sort_by { |a| a.first }

  # Log events
  sorted_lines.each do |(datetime, event)|
    if WAKES_UP.eql? event
      current_guard.log_sleep!(current_sleep_start, datetime.minute)
    elsif FALLS_ASLEEP.eql? event
      current_sleep_start = datetime.minute
    else
      # new guard on duty
      current_guard = guards[event.to_i]
    end
  end.compact.sort

  sleepiest_overall = guards.values.inject([nil, -1, -1]) do |(winning_guard, sleepiest_minute, times_slept), current_guard|
    max_minute = current_guard.sleep_minutes.max

    if max_minute > times_slept
      [current_guard, current_guard.sleep_minutes.each_with_index.max.last, max_minute]
    else
      [winning_guard, sleepiest_minute, times_slept]
    end
  end

  sleepiest_guard, sleepiest_minute, times_slept_that_minute = *sleepiest_overall


  puts "Guard: ##{sleepiest_guard.id} slept #{times_slept_that_minute} times at minute #{sleepiest_minute}"
  puts "Puzzle answer  : #{sleepiest_guard.id * sleepiest_minute}"
end

