#!/usr/bin/env ruby
#
# AoC 2018 Day 4 part 1
# (c)2018 Ross Bamford
# See LICENSE
#
require 'date'

module AoC
  module Day4

    # A guard
    class Guard
      attr_reader :id, :sleep_minutes, :total_minutes_sleep

      def initialize(id)
        @id, @sleep_minutes, @total_minutes_sleep = id, [0] * 60, 0
      end

      # Log a sleep period for this guard, from start_minute to end_minute (exclusive).
      def log_sleep!(start_minute, end_minute)
        (start_minute...end_minute).each { |m| @sleep_minutes[m] += 1 }
        @total_minutes_sleep += end_minute - start_minute
      end

      # Returns frequency of minutes slept in the the hour, in descending order
      # of frequency.
      #
      # For example, [[3, 10], [2, 5]] means that this guard was caught napping
      # 3 times at 00:10, and twice at 00:05.
      def sleep_minutes_frequency
        sleep_minutes.each_with_index.sort.reverse
      end

      # The minute past the hour in which this guard was most frequently asleep.
      def sleepiest_minute
        sleep_minutes_frequency.first.last
      end
    end

    # An event (shift change, fall asleep or wake up)
    class Event
      class << self
        class ParseResult
          def initialize(event)
            @event = event
          end

          def and_then
            yield @event unless @event.nil?
            self
          end

          def or_else
            yield if @event.nil?
            self
          end
        end

        # Try to parse the given line as an event.
        # Returns a ParseResult for further processing.
        def try_parse(line)
          ParseResult.new(
            if line =~ /\[([^\]]+)\]\s(.*)/
              Event.parse($1,$2)
            else
              nil
            end
          )
        end

        # Create a new event by parsing the given timestamp
        # and event strings, which must be in the puzzle format.
        def parse(timestamp_str, event_str)
          timestamp =  DateTime.parse(timestamp_str)
          event = if event_str =~ /Guard\s#(\d+)/
            $1
          else
            event_str
          end

          Event.new(timestamp, event)
        end
      end

      include Comparable

      attr_reader :timestamp, :event

      def initialize(timestamp, event)
        @timestamp, @event = timestamp, event
      end

      def <=>(other)
        @timestamp <=> other.timestamp
      end
    end

    # Tracks guards' sleep patterns while on duty, and allows the sleepiest
    # guard to be determined once all events are logged.
    class SleepLog
      attr_reader :sleepiest_guard

      def initialize(guards = Hash.new { |h,k| h[k] = Guard.new(k) })
        @guards, @current_guard, @current_sleep_start, @events = guards, nil, nil, []
        yield self
        calculate!
      end
      
      # Log an event.
      # Should be called only from block passed to constructor
      def event!(event)
        @events << event
      end

      private
      
      def calculate!
        @events.sort.each do |event|
          if WAKES_UP.eql? event.event
            @current_guard.log_sleep!(@current_sleep_start, event.timestamp.minute)
          elsif FALLS_ASLEEP.eql? event.event
            @current_sleep_start = event.timestamp.minute
          else
            # new guard on duty
            @current_guard = @guards[event.event.to_i]
          end
        end

        @events = nil
        @sleepiest_guard = @guards.values.sort_by { |guard| guard.total_minutes_sleep }.last
        freeze
      end

      FALLS_ASLEEP = "falls asleep"
      WAKES_UP     = "wakes up"
    end
  end
end

if $0 == __FILE__
  include AoC::Day4

  # Log events
  log = SleepLog.new do |log|
    File.read(ARGV[0] || 'input.txt').each_line.map do |line|
      Event.try_parse(line)
        .and_then { |event| log.event!(event) }
        .or_else  { STDERR << "Failed to parse '#{line}'" }
    end
  end

  sleepiest_guard = log.sleepiest_guard
  sleepiest_minute = sleepiest_guard.sleepiest_minute

  puts "Sleepiest guard: ##{sleepiest_guard.id}; Most likely minute: #{sleepiest_minute}"
  puts "Puzzle answer  : #{sleepiest_guard.id * sleepiest_minute}"
end


