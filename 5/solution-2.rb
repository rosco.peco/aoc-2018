#!/usr/bin/env ruby
#
# AoC 2018 Day 5 part 2
# (c)2018 Ross Bamford
# See LICENSE
#
# Totally non-optimal but quick to write ;)
#
require './polymer_reactor.rb'

if $0 == __FILE__
  include AoC::Day5

  input = File.read(ARGV[0] || 'input.txt')
  result_lens = []

  result_lens = ('A'..'Z').map { |c| /#{c + '|' + c.downcase}/ }.reduce([[], 0]) do |(results, done), remove_pair|
    printf "\rProcessing (%00.2f%%)", done
    results << PolymerReactor.obliterate!(input.gsub(remove_pair, '')).length
    [results, done + 100.0 / 26]
  end.first

  puts "\rShortest: #{result_lens.min}                            "
end

