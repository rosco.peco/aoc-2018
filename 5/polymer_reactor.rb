# AoC 2018 Day 5 common code
# (c)2018 Ross Bamford
# See LICENSE
#
# Totally non-optimal but quick to write ;)
#
module AoC
  module Day5
    class PolymerReactor
      class << self
        def obliterate!(polymer)
          new_polymer = polymer.gsub(RX, '')

          if new_polymer.length == polymer.length
            new_polymer
          else
            obliterate!(new_polymer)
          end
        end

        private 

        RX = /#{('A'..'Z').map { |c| c + c.downcase }.map { |pair| [pair, pair.reverse] }.flatten.join('|')}/
      end
    end
  end
end
