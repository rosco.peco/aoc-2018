#!/usr/bin/env ruby
#
# AoC 2018 Day 5 part 1
# (c)2018 Ross Bamford
# See LICENSE
#
# Totally non-optimal but quick to write ;)
#
require './polymer_reactor.rb'

if $0 == __FILE__
  include AoC::Day5
  input = File.read(ARGV[0] || 'input.txt')
  puts "Length: #{PolymerReactor.obliterate!(input).length}"
end


