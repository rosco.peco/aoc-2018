#!/usr/bin/env ruby
#
# AoC 2018 Day 1 part 1
# (c)2018 Ross Bamford
# See LICENSE
#
if $0 == __FILE__
  puts File.read(ARGV[0] || 'input.txt').split.map(&:to_i).reduce(:+)
end
