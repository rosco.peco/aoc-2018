#!/usr/bin/env ruby
#
# AoC 2018 Day 1 part 2
# (c)2018 Ross Bamford
# See LICENSE
#
module AoC
  module Day1
    class << self
      def first_dup_freq(aints)
        # first run through
        total, seen = aints.inject([0,{0 => true}]) do |(s,h),i|
          sum = s + i
          h[sum] = true
          [sum, h]
        end

        # continue
        loop do
          # special case
          if total == 0
            return 0
          end    

          aints.each do |i|
            if seen[total += i]
              return total
            else
              seen[total] = true
            end
          end
        end
      end
    end
  end
end

if $0 == __FILE__
  puts AoC::Day1::first_dup_freq(File.read(ARGV[0] || 'input.txt').split.map(&:to_i))
end
