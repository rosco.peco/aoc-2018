#!/usr/bin/env ruby
#
# AoC 2018 Day 16 part 1
# (c)2018 Ross Bamford
# See LICENSE

module AoC
  module Day16
    class RM
      def initialize(regs = [0, 0, 0, 0])
        @regs = regs
      end

      def regs
        @regs
      end
    end
  end
end

if $0 == __FILE__
  include AoC::Day16

  OPS = [
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] + rm.regs[b] },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] + b },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] * rm.regs[b] },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] * b },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] & rm.regs[b] },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] & b },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] | rm.regs[b] },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] | b },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = a },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = a > rm.regs[b] ? 1 : 0 },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] > b ? 1 : 0 },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] > rm.regs[b] ? 1 : 0 },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = a == rm.regs[b] ? 1 : 0 },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] == b ? 1 : 0 },
    lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] == rm.regs[b] ? 1 : 0 },
  ]

  inputs = File.read('input-1.txt').each_line
      .map { |l| l.chomp }
      .reject(&:empty?)
      .to_a.each_slice(3)
      .map { |a| a.map do |l| 
        if l =~ /[A-Z][a-z]+:\s+\[([^\]]+)/ 
           $1.split(', ')
        else
          l.split(' ')
        end
      end }
      .map do |group|
        group.map do |n|
          n.map(&:to_i)
        end
      end

  i = 0

  results = inputs.map do |(before, insn, after)|
    OPS.each_with_index.map do |op, original_num|
      rm = RM.new(before.dup)
      op.call(rm, insn) 
      [original_num, rm.regs]
    end.select { |(original_num, output)| output == after }
  end.select { |result| result.length > 2 }.length

  puts "Puzzle answer: #{results}"
end

