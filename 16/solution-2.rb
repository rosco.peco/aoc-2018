#!/usr/bin/env ruby
#
# AoC 2018 Day 16 part 2
# (c)2018 Ross Bamford
# See LICENSE

module AoC
  module Day16
    class RM
      def initialize(regs = [0, 0, 0, 0])
        @regs = regs
      end

      def regs
        @regs
      end
    end
  end
end

if $0 == __FILE__
  include AoC::Day16

  VERBOSE = ARGV.delete('--verbose')  

  OPS = {
    :addr => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] + rm.regs[b] },
    :addi => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] + b },
    :mulr => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] * rm.regs[b] },
    :muli => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] * b },
    :banr => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] & rm.regs[b] },
    :bani => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] & b },
    :borr => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] | rm.regs[b] },
    :bori => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] | b },
    :setr => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] },
    :seti => lambda { |rm, (op, a, b, c)| rm.regs[c] = a },
    :gtir => lambda { |rm, (op, a, b, c)| rm.regs[c] = a > rm.regs[b] ? 1 : 0 },
    :gtri => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] > b ? 1 : 0 },
    :gtrr => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] > rm.regs[b] ? 1 : 0 },
    :eqir => lambda { |rm, (op, a, b, c)| rm.regs[c] = a == rm.regs[b] ? 1 : 0 },
    :eqri => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] == b ? 1 : 0 },
    :eqrr => lambda { |rm, (op, a, b, c)| rm.regs[c] = rm.regs[a] == rm.regs[b] ? 1 : 0 },
  }

  # Input from part 1 is the training input
  inputs = File.read('input-1.txt').each_line
      .map { |l| l.chomp }
      .reject(&:empty?)
      .to_a.each_slice(3)
      .map { |a| a.map do |l| 
        if l =~ /[A-Z][a-z]+:\s+\[([^\]]+)/ 
           $1.split(', ')
        else
          l.split(' ')
        end
      end }
      .map do |group|
        group.map do |n|
          n.map(&:to_i)
        end
      end

  # Map sample data into a set of possibilities
  possibles = Array.new(OPS.size) { OPS.keys }
  count = inputs.select do |(before, insn, after)|
    similar = OPS.select do |mnemonic, op|
      rm = RM.new(before.dup)
      op.call(rm, insn) 
      rm.regs == after
    end
    possibles[insn.first] &= similar.keys
    similar.size > 2
  end.length
  
  # Try to reprogram VM
  new_ops = []
  until new_ops.length == 16 && new_ops.all? 
    single_match_index = possibles.index { |results| results.length == 1 }
    raise "Too many possibilities: #{possibles}" unless single_match_index
  
    mnemonic = possibles[single_match_index].first
    puts "New opcode ##{single_match_index} is #{mnemonic.inspect}" if VERBOSE
    new_ops[single_match_index] = OPS[mnemonic]
    possibles.each { |e| e.delete(mnemonic) }
  end

  # Run the program?
  program = File.read('input-2.txt').each_line.map { |s| s.split.map(&:to_i) }

  rm = RM.new([0, 0, 0, 0])
  program.each { |insn| new_ops[insn.first].call(rm, insn) }
  
  puts "Final registers: #{rm.regs}; Puzzle answer: #{rm.regs[0]}"
end

