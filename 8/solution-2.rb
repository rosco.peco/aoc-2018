#!/usr/bin/env ruby
#
# AoC 2018 Day 8 part 2
# (c)2018 Ross Bamford
# See LICENSE
#
require './solution-1.rb'

module AoC
  module Day8
    class Node
      def node_value
        if self.kids.empty?
          self.metadata.sum
        else
          kid_values = self.kids.map { |kid| kid.node_value }
          self.metadata.map { |md| kid_values[md-1] }.compact.sum
        end
      end
    end
  end
end

if $0 == __FILE__
  puts "Value: #{AoC::Day8::Node::read_node(File.read(ARGV[0] || 'input.txt').split.map(&:to_i)).node_value}" 
end

