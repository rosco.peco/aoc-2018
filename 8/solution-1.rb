#!/usr/bin/env ruby
#
# AoC 2018 Day 8 part 1
# (c)2018 Ross Bamford
# See LICENSE
#
module AoC
  module Day8
    class Node
      class << self
        def read_node(input, parent = nil)
          child_count, metadata_size = input.shift, input.shift
          node = Node.new(child_count, metadata_size)

          node.kids.map! { read_node(input) }
          node.metadata.map! { input.shift }

          node
        end
      end

      attr_reader :kids, :metadata

      def initialize(child_count, metadata_size)
        @kids, @metadata = Array.new(child_count), Array.new(metadata_size)
      end

      def traverse_breadth_first(state)
        queue = [self]

        until queue.empty?
          current = queue.shift
          state = yield state, current
          queue.push(*current.kids)
        end

        state
      end
    end
  end
end

if $0 == __FILE__
  require 'pp'

  include AoC::Day8

  input = File.read(ARGV[0] || 'input.txt').split.map(&:to_i)

  # breadth-first traversal
  metadata = Node::read_node(input).traverse_breadth_first([]) do |state, node|
    state + node.metadata
  end

  puts "#{metadata.join('+')} = #{metadata.sum}" 
end

