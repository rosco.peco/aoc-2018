#!/usr/bin/env ruby
#
# AoC 2018 Day 15 part 1
# (c)2018 Ross Bamford
# See LICENSE

module AoC
  module Day15
    module Positioned
      attr_accessor :x, :y
      # N.B. In "reading" order (T->B, L->R)
      def surrounding_positions
        [[x, y - 1], [x - 1, y], [x + 1, y], [x, y + 1]]
      end
    end

    class Character
      include Positioned

      attr_reader :id, :attack_power
      attr_accessor :hit_points
      
      def initialize(x, y, attack_power, hit_points)
        @x, @y, @attack_power, @hit_points = x, y, attack_power, hit_points
      end

      def choose_victim(potentials)
        potentials.inject(potentials.shift) do |victim, char|
          if char.hit_points < victim.hit_points
            char
          else
            victim
          end
        end
      end

      def move!(new_tile)
        @x, @y = new_tile.x, new_tile.y
      end
    end

    class Goblin < Character
      def self.reset_count!
        @@count = 0
      end

      def initialize(x, y)
        super(x, y, 3, 200)
        @@count ||= 0
        @id = @@count += 1
      end

      def enemy_of?(other)
        !other.is_a? Goblin
      end
        
      def to_map_s
        "\e[1;32mG\e[0m"
      end

      def to_s
        "\e[1;32mG#{id}\e[0m"
      end

      def race
        "\e[1;32mGoblins\e[0m"
      end
    end

    class Elf < Character
      def self.reset_count!
        @@count = 0
      end

      def initialize(x, y, attack_power = 3)
        super(x, y, attack_power, 200)
        @@count ||= 0
        @id = @@count += 1
      end

      def enemy_of?(other)
        !other.is_a? Elf
      end

      def to_map_s
        "\e[1;31mE\e[0m"
      end

      def to_s
        "\e[1;31mE#{id}\e[0m"
      end

      def race
        "\e[1;31mElves\e[0m"
      end
    end

    class Tile
      include Positioned

      def initialize(x, y)
        @x, @y = x, y
      end
    end

    class WalkableTile < Tile
      def initialize(x, y)
        super
      end

      def walkable?
        true
      end

      def to_s 
        '.'
      end
    end

    class WallTile < Tile
      def initialize(x, y)
        super
      end

      def walkable?
        false
      end

      def to_s
        '#'
      end
    end

    class PathNode
      attr_accessor :tile, :parent

      def initialize(tile, parent = nil)
        @tile, @parent = tile, parent
      end

      def ==(other)
        @tile == other.tile
      end

      def eql?(other)
        @tile.eql? other.tile
      end

      def hash
        @tile.hash
      end

      def inspect
        "NODE[#{tile.x}, #{tile.y}]"
      end
    end

    class Map
      attr_reader :array, :characters

      def initialize(text_map, elf_power = 3)
        raise ArgumentError.new("Map must be uniform width") unless text_map.all? { |line| line.length == text_map[0].length }

        # Find all characters, and remove them from the text map (replacing with walkable tiles)
        map, @characters = text_map.each_with_index.inject([[],[]]) do |(map, characters),(line, y)|
          map << line.each_with_index.inject([[], characters]) do |(mapline,characters),(cell, x)|
            if cell == 'G'
              characters << Goblin.new(x, y)
              mapline << '.'
            elsif cell == 'E'
              characters << Elf.new(x, y, elf_power)
              mapline << '.'
            else
              mapline << cell
            end

            [mapline, characters]
          end.first

          [map, characters]
        end
        
        @array = map.each_with_index.inject([]) do |array, (line, y)|
          array << line.each_with_index.inject([]) do |line_array, (cell, x)|
            case cell
            when '.' then line_array << WalkableTile.new(x, y)
            when '#' then line_array << WallTile.new(x, y)
            else raise ArgumentError.new("Unrecognised character '#{cell}' in input array!")
            end
          end
        end

        recompute_character_maxes!
        @map_max_x, @map_max_y = @array[0].length - 1, @array.length - 1
      end

      def display!(i, map, cls = !VERBOSE)
        puts "#{"\e[2J\e[;H" if cls}ITERATION #{i}"
        puts self
        puts "\e[#{self.array.length}A"
  
        chars = self.all_sorted_characters 
        chars.each do |char|
          puts "\e[#{self.array.first.length + 10}C#{char} is at (#{char.x}, #{char.y}) with #{char.hit_points} remaining"
        end
  
        (map.array.length - chars.length + 1).times { puts }
      end
  

      def characters_at(x, y)
        @characters.select { |char| char.x == x && char.y == y }
      end

      def all_sorted_characters(subset = @characters)
        a = []
        (@min_character_y..@max_character_y).each do |y|
          subset.select { |char| char.y == y }.sort_by { |char| char.x }.each { |char| a << char }
        end
        a
      end

      def each_sorted_character(subset = @characters, &blk)
        all_sorted_characters(subset).each(&blk)
      end

      def first_sorted_character(subset = @characters)
        each_sorted_character(subset) { |char| return char if yield(char) }
      end

      def has_enemies?(character)
        !sorted_enemies_of(character).empty?
      end

      def sorted_enemies_of(character)
        all_sorted_characters.select { |other| other.enemy_of?(character) }
      end

      def character_tile(character)
        @array[character.y][character.x]
      end

      def vacant_walkable_surrounding_positions(x, y, always_include = [])
        @array[y][x].surrounding_positions.reject do |(x,y)| 
          !always_include.include?([x,y]) and x < 0 || x > @map_max_x || y < 0 || y > @map_max_y || !@array[y][x].walkable? || !characters_at(x,y).empty?
        end
      end

      def vacant_walkable_surrounding_tiles(x, y, always_include_tiles = [])
        vacant_walkable_surrounding_positions(x, y, always_include_tiles.map { |t| [t.x, t.y] }).map { |(x,y)| @array[y][x] }
      end

      def attackable_characters(character)
        @array[character.y][character.x].surrounding_positions.map do |(x,y)| 
          characters_at(x,y)
        end.select do |others|
          !others.empty? and others.all? { |other| other.enemy_of?(character) }
        end.flatten
      end

      def remove_character!(character)
        @characters.delete(character)
      end

      def shortest_path(from_tile, to_tile)
        build_path(bfs_compute_parents(from_tile, to_tile), from_tile)
      end

      def find_next_move(character)
        paths = sorted_enemies_of(character).map { |enemy| shortest_path(character_tile(character), character_tile(enemy)) }.reject { |a| a.empty? }

        if paths.length > 0
          paths = paths.sort_by(&:length)
          shortest_paths = paths.select { |p| p.length == paths.first.length }

          p = if shortest_paths.length > 1
            shortest_paths.sort_by { |path| path.last.x + (path.last.y * @array.first.length) }.first
          else
            shortest_paths.first
          end
          
          p[1]
        else
          p = nil
        end

      end

      def to_s
        @array.each_with_index.inject("") do |s,(line, y)|
          line.each_with_index.inject(s) do |s,(cell, x)|
            if (chars = characters_at(x,y)).empty?
              s << cell.to_s
            else
              s << chars.first.to_map_s
            end
          end + "\n"
        end
      end

      def play_round!(round_number)
        dead_this_turn = []

        self.each_sorted_character do |char|
          if dead_this_turn.include?(char)
            puts "The #{['bloodied', 'rotting', 'already-effluvient', 'good-looking, though foul-smelling', 'radiant', 'oddly-vocal'].sort_by { rand }.first} corpse of #{char} is trodden deeper into the miasma" if VERBOSE
          else
  
            puts "It is #{char}'s turn!" if VERBOSE
  
            if !self.has_enemies?(char)
              puts "#{char.to_s} looks around, and finds all his enemies are vanquished; It has been a bloody \e[1;33m#{round_number-1} full iterations\e[0m, but the #{char.race} have finally won the day."
              return true
            end
  
            potential_victims = self.attackable_characters(char)
  
            if potential_victims.empty?
              # move
              if new_tile = self.find_next_move(char)
                char.move!(new_tile)
                potential_victims = self.attackable_characters(char)
  
                puts "#{char} #{['moves', 'sashays', 'dashes', 'wanders', 'stumbles', 'star-jumps', 'bounces', 'slithers', 'crawls', 'flies first-class', 'sidles', 'creeps'].sort_by { rand }.first} to #{char.x}, #{char.y}" if VERBOSE
              else
                puts "#{char} cannot reach an enemy, and so #{['howls in impotent rage', 'roars a mighty roar of encouragement', 'dances a jig', 'does some cross-stitch', 'does the Sunday Times crossword'].sort_by { rand }.first} instead" if VERBOSE
              end
            end
  
            unless potential_victims.empty?
              # attack!
              victim = char.choose_victim(potential_victims)
              puts "#{char} attacks #{victim} with #{['an incongruously-available sub-machine gun', 'a sword', 'an axe', 'a spear', 'a dagger', 'a feather-duster', 'a ham sandwich', 'bad breath'].sort_by { rand }.first}" if VERBOSE
  
              victim.hit_points -= char.attack_power
              
              if victim.hit_points <= 0
                puts "#{victim} keels over and dies #{['honourably', 'quietly', 'slowly', 'gruesomely', 'flatulently', 'gratuitously', 'in vain', 'with a Wilhelm scream'].sort_by { rand }.first}" if VERBOSE
                self.remove_character!(victim)
                dead_this_turn << victim
              else
                puts "#{victim} is wounded, and#{' only' if victim.hit_points < 20} has #{victim.hit_points} hit-points left" if VERBOSE
              end
            end
          end
  
          recompute_character_maxes!
        end

        false
      end

      private

      def recompute_character_maxes!
        charys = @characters.map { |char| char.y }
        @min_character_y, @max_character_y = charys.min, charys.max
      end

      def bfs_compute_parents(from, to)
        closed = {}
        open = [PathNode.new(from)]

        until open.empty?
          closed[current = open.shift] = true
          neighbours = vacant_walkable_surrounding_tiles(current.tile.x, current.tile.y, [to])
            .map { |tile| PathNode.new(tile) }
            .reject { |node| closed[node] || open.include?(node) }
          
          neighbours.each { |node| node.parent = current }
          open += neighbours
         
          return current if current.tile == to
        end

        return nil
      end

      def build_path(path_node, from)
        path = []

        until path_node.nil?
          path.unshift(path_node.tile)
          path_node = path_node.parent
        end

        path
      end
    end
  end
end

if $0 == __FILE__
  VERBOSE = ARGV.delete('--verbose')
  SINGLE_STEP = ARGV.delete('--debug')

  include AoC::Day15

  # Create the map
  map = Map.new(File.read(ARGV[0] || 'test1-1.txt').each_line.map { |line| line.strip.split('') })
 
  # Play!
  last_iter = catch(:done) do
    (1..Float::INFINITY).each do |i|
      map.display!(i - 1, map)

      if map.play_round!(i)
        throw(:done, i - 1)
      end

      if (SINGLE_STEP)
        print 'Hit return...'
        STDIN.readline if SINGLE_STEP
      end
    end
  end

  print "\n\nTHE AFTERMATH: "
  map.display!(last_iter + 1, map, false)

  puts "Survivors: #{map.characters.join(", ")}"
  puts "Finished during iteration #{last_iter + 1}; Last full iteration was \e[1;33m#{last_iter}\e[0m"
  remaining_points = map.characters.inject(0) { |sum, char| sum += char.hit_points }
  puts "\e[1;33m#{remaining_points}\e[0m hit points remain" 
  puts "Puzzle solution is \e[1;31m#{last_iter * remaining_points}\e[0m"
end

