#!/usr/bin/env ruby
#
# AoC 2018 Day 15 part 2
# (c)2018 Ross Bamford
# See LICENSE

if $0 == __FILE__
  require './solution-1.rb'

  VISUAL = ARGV.delete('--visual')
  VERBOSE = ARGV.delete('--verbose') && VISUAL
  SINGLE_STEP = ARGV.delete('--debug') && VISUAL

  include AoC::Day15

  (3..Float::INFINITY).each do |elf_power|
    [Elf, Goblin].each { |type| type.reset_count! }

    # Create the map
    map = Map.new(File.read(ARGV[0] || 'test1-1.txt').each_line.map { |line| line.strip.split('') }, elf_power)

    starting_elf_count = map.characters.inject(0) { |sum, character| character.is_a?(Elf) ? sum +=1 : sum }
    puts "There are #{starting_elf_count} elves at the beginning with power (#{elf_power})"
    
    # Play!
    catch(:abandon_iter) do
      last_iter = catch(:done) do
        (1..Float::INFINITY).each do |i|
          map.display!(i - 1, map) if VISUAL

          if map.play_round!(i)
            throw(:done, i - 1)
          end

          elf_count = map.characters.inject(0) { |sum, character| character.is_a?(Elf) ? sum +=1 : sum }
          if elf_count < starting_elf_count
            puts "An elf died; Abandoning this fight!"
            throw(:abandon_iter)
          end
        end
      end

      print "THE AFTERMATH: "
      display!(last_iter + 1, map, false) if VISUAL

      puts "Survivors: #{map.characters.join(", ")}"
      puts "Finished during iteration #{last_iter + 1}; Last full iteration was \e[1;33m#{last_iter}\e[0m"
      remaining_points = map.characters.inject(0) { |sum, char| sum += char.hit_points }
      puts "\e[1;33m#{remaining_points}\e[0m hit points remain" 
      puts "Puzzle solution is \e[1;31m#{last_iter * remaining_points}\e[0m"

      surviving_elf_count = map.characters.inject(0) { |sum, character| character.is_a?(Elf) ? sum +=1 : sum }
      puts "There are #{surviving_elf_count} elves at the end with power (#{elf_power})"

      if surviving_elf_count == starting_elf_count 
        puts "All elves survived. They require attack power of \e[1;31m#{elf_power}\e[0m in order to take no casualties"
        exit!
      else
        puts "There were #{starting_elf_count - surviving_elf_count} elf casualties; Rerolling with next power level..."
        puts
      end
    end
  end
end

