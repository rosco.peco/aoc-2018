#!/usr/bin/env ruby
#
# AoC 2018 Day 3 part 1
# (c)2018 Ross Bamford
# See LICENSE
#
# This is a second Hash-based solution to part 1. 
# It trades time for space compared to the main solution.
# 
# For larger fabrics/datasets, something like this would be 
# the better solution.
#
module AoC
  module Day3
    class Rectangle
      attr_accessor :top, :left, :width, :height

      def initialize(left, top, width, height)
        @top, @left, @width, @height = top, left, width, height
      end

      def each_point
        (top...top + height).each do |y|
          (left...left + width).each do |x|
            yield x,y
          end
        end
      end
    end

    class Fabric
      attr_accessor :overlap_inches

      def initialize
        @fabric, @overlap_inches = Hash.new { |h,k| h[k] = 0 }, 0
      end

      def use(x,y)
        key = "#{x},#{y}"

        result = @fabric[key] += 1

        if result == 2
          @overlap_inches += 1
        end
      end
    end
  end
end

if $0 == __FILE__
  fabric = AoC::Day3::Fabric.new

  rects = File.read(ARGV[0] || 'input.txt').each_line.map do |line|
    if line =~ /#\d+\s+@\s+(\d+),(\d+):\s+(\d+)x(\d+)/
      AoC::Day3::Rectangle.new($1.to_i, $2.to_i, $3.to_i, $4.to_i)
    else
      nil
    end
  end.compact

  rects.each do |rect|
    rect.each_point do |x,y|
      fabric.use(x,y) 
    end
  end

  puts "Overlapping inches: #{fabric.overlap_inches}"
end

