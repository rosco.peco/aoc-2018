#!/usr/bin/env ruby
#
# AoC 2018 Day 3 part 1
# (c)2018 Ross Bamford
# See LICENSE
#

module AoC
  module Day3
    class Rectangle
      attr_reader :left, :top, :width, :height

      def initialize(left, top, width, height)
        @left, @top, @width, @height = left, top, width, height
      end

      def each_point
        (top...top + height).each do |y|
          (left...left + width).each do |x|
            yield x,y
          end
        end
      end
    end

    class Fabric
      attr_reader :overlap_inches

      def initialize(width = 2000)
        @width, @fabric, @overlap_inches = width, [], 0
      end

      def add_rect!(rect)
        rect.each_point do |x,y|
          use!(x,y) 
        end
      end

      alias << add_rect!

      private

      def point_to_linear(x,y)
        x + y * @width
      end
      
      def use!(x,y)
        linear = point_to_linear(x,y)

        @fabric[linear] = (@fabric[linear] || 0) + 1 

        if @fabric[linear] == 2
          @overlap_inches += 1
        end
      end
    end
  end
end

if $0 == __FILE__
  include AoC::Day3

  fabric = Fabric.new

  File.read(ARGV[0] || 'input.txt').each_line.map do |line|
    if line =~ /#\d+\s+@\s+(\d+),(\d+):\s+(\d+)x(\d+)/
      fabric << Rectangle.new($1.to_i, $2.to_i, $3.to_i, $4.to_i)
    end
  end

  puts "Overlapping inches: #{fabric.overlap_inches}"
end

