#!/usr/bin/env ruby
#
# AoC 2018 Day 6 part 1
# (c)2018 Ross Bamford
# See LICENSE
#
if $0 == __FILE__
  md = lambda { |(x1,y1),(x2,y2)| (x1 - x2).abs + (y1 - y2).abs }

  input = File.read('input.txt').each_line.map { |line| line.scan(/\d+/).map(&:to_i) }.reject(&:empty?)
  areas = Hash.new { |h,k| h[k] = {} }

  ymin = input.map(&:last).min
  ymax = input.map(&:last).max
  xmin = input.map(&:first).min
  xmax = input.map(&:first).max	

  (xmin..xmax).each do |x|
    (ymin..ymax).each do |y|
      dists = input.each_with_index.map { |point, i| [md.call(point, [x,y]), i] }.sort_by(&:first)
      areas[x][y] = dists[0][1] if dists[0].first != dists[1].first
    end
  end

  counts = (xmin..xmax).inject([0] * input.length) do |r, x|
    (ymin..ymax).inject(r) do |r, y|
      if areas[x][y]
        r[areas[x][y]] += 1
      end
      r
    end
  end

  puts "Largest area: #{counts.max}"
end

