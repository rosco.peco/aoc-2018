#!/usr/bin/env ruby
#
# AoC 2018 Day 6 part 1
# (c)2018 Ross Bamford
# See LICENSE
#
if $0 == __FILE__
  MAX_DISTANCE = 10000

  md = lambda { |(x1,y1),(x2,y2)| (x1 - x2).abs + (y1 - y2).abs }

  input = File.read('input.txt').each_line.map { |line| line.scan(/\d+/).map(&:to_i) }.reject(&:empty?)
  areas = Hash.new { |h,k| h[k] = {} }

  ymin = input.map(&:last).min
  ymax = input.map(&:last).max
  xmin = input.map(&:first).min
  xmax = input.map(&:first).max	

  average = MAX_DISTANCE / input.size

  count = ((xmin - average)..(xmax + average)).inject(0) do |c,x|
    ((ymin - average)..(ymax + average)).inject(c) do |c,y|
      c += input.map { |p| md[[x,y], p] }.reduce(&:+) < MAX_DISTANCE ? 1 : 0
    end
  end

  puts "Total distance: #{count}"
end

