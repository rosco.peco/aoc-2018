#!/usr/bin/env ruby
#
# AoC 2018 Day 10 character recognition
#
# Currently only recognises characters from my personal puzzle input,
# as I don't know what other characters look like, obviously...
#
# (c)2018 Ross Bamford
# See LICENSE

module AoC
  module Day10
    class Character
      class << self 
        def [](*chrdata)
          new(chrdata)
        end

        def encode(eight_by_n_strings)
          eight_by_n_strings.map { |s| s.split(//).each_with_index.inject(0) { |bmp, (c, i)|  c.eql?('*') && bmp | 1 << i or bmp } }
        end
      end

      def initialize(*chrdata) 
        @chrdata = chrdata
      end


      def match?(string_array)
        Character.encode(string_array)
          .zip(*@chrdata)
          .partition
          .all? { |s,d| s == d }
      end
    end

    CHARACTERS = [ 
      Character[],
      Character[31, 33, 33, 33, 31, 33, 33, 33, 33, 31],
      Character[],
      Character[],
      Character[],
      Character[63, 1, 1, 1, 31, 1, 1, 1, 1, 1],
      Character[30, 33, 1, 1, 1, 57, 33, 33, 49, 46],
      Character[],
      Character[],
      Character[],
      Character[],
      Character[1, 1, 1, 1, 1, 1, 1, 1, 1, 63],
      Character[],
      Character[],
      Character[],
      Character[31, 33, 33, 33, 31, 1, 1, 1, 1, 1],
      Character[],
      Character[31, 33, 33, 33, 31, 9, 17, 17, 33, 33]
    ]
  end
end

if $0 == __FILE__
  include AoC::Day10

  require 'test/unit'

  class TestCharacter < Test::Unit::TestCase
    def test_empty_char
      char = ["        "] * 10
      assert_equal [0] * 10, Character.encode(char)
    end

    def test_full_char
      char = ["        "] * 10
      assert_equal [0] * 10, Character.encode(char)
    end

    def test_each_line
      char = [
        "        ", # 0
        "*       ", # 1
        "**      ", # 3
        "***     ", # 7
        "****    ", # 15
        "*****   ", # 31
        "******  ", # 63
        "******* ", # 127
        "********", # 255
        " *******", # 254
        "  ******", # 252
        "   *****", # 248
        "    ****", # 240
        "     ***", # 224
        "      **", # 192
        "       *", # 128
        "*    *  ", # 33
        " ****   ", # 30
        "*  ***  ", # 57
        "*   **  ", # 49
        " *** *  ", # 46
        "*  *    ", # 9
        "*   *   ", # 17
      ]

      assert_equal [
        0,   1,   3,   7,   15,  31,  63, 127, 255, 254,
        252, 248, 240, 224, 192, 128, 33, 30, 57, 49, 46,
        9, 17
      ], Character.encode(char)
    end
  end
end



