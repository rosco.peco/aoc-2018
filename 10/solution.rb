#!/usr/bin/env ruby
#
# AoC 2018 Day 10 parts 1 & 2
# (c)2018 Ross Bamford
# See LICENSE
#
# In solving part one, I also unwittingly solved problem two :) 
# For this to work, you need a small font in your terminal (Consolas 8 point worked for me)

module AoC
  module Day10
    class Point
      attr_reader :x, :y, :deltax, :deltay

      def initialize(args)
        @x, @y, @deltax, @deltay = *args.map { |a| a.to_i }
      end

      def position_after_seconds(secs)
        [self.x + self.deltax * secs, self.y + self.deltay * secs]
      end
    end

    class Viewport
      attr_reader :extents, :stars, :output

      def initialize(extents, stars)
        @extents, @stars = extents, stars
        draw!
      end

      def num_chars
        @output[0].length / 8 + 1
      end

      def char_at(i)
        @output.map { |s| s[(i * 8)..(i * 8 + 7)] }
      end

      def inject(state)
        num_chars.times do |i|
          yield state, char_at(i)
        end
        state
      end

      private

      def draw!
        startx, endx, starty, endy = *self.extents
        viswidth = endx - startx

        output = (starty..endy).map do |y|
          line = self.stars.map { |(sx,sy)| sx if y == sy }.compact

          unless line.empty?
            (startx..endx).inject("") do |str, x|
              if line.include?(x)
                str << "*"
              else
                str << " "
              end
            end
          end
        end.compact

        leading_space = output.map { |s| s =~ /[^\s]/ }.min
        @output = output.map { |s| s[leading_space..-1] }
      end			
    end		
  end
end

if $0 == __FILE__
  require './characters.rb'

  include AoC::Day10

  RX = /position=<([\-\s\d]+), ([\-\s\d]+)> velocity=<([\-\s\d]+), ([\-\s\d]+)>/
  MAX_SECONDS = 11000

  points = File.read(ARGV[0] || 'input.txt').scan(RX).map(&Point.method(:new))

  extents, sizes = MAX_SECONDS.times.inject([[],[]]) do |(extents, sizes), t|
    positions = points.map { |p| p.position_after_seconds(t) }
    minx, maxx, miny, maxy = *positions.inject([0,0,0,0]) do |(minx, maxx, miny, maxy), p|
      minx = p.first if p.first < minx
      maxx = p.first if p.first > maxx
      miny = p.last if p.last < miny
      maxy = p.last if p.last > maxy
      [minx, maxx, miny, maxy]
    end

    [
      extents << [minx, maxx, miny, maxy],
      sizes << (maxx - minx) * (maxy - miny)
    ]
  end

  aligned_second = sizes.each_with_index.sort_by(&:first).first.last

  if aligned_second == MAX_SECONDS - 1
    puts "The stars (probably) didn't align within the time frame; increase MAX_SECONDS and try again"
  else
    puts "Stars aligned at #{aligned_second} seconds"
    view = Viewport.new(extents[aligned_second], points.map { |p| p.position_after_seconds(aligned_second) })

    chars = view.inject("") do |state, char|
      if ascii = (CHARACTERS.each_with_index.find { |c, i| c.match?(char) } || []).last
        state << (65 + ascii).chr
      else
        state
      end
    end

    puts "Message: #{chars}"
  end
end

